#!/usr/bin/perl

use strict;
use warnings;



my $dir = $ARGV[0];

# these will be used by highlight subroutine
my %cmrules = loadrules();
my $spanlen = length('<span class=""></span>');

my @pre;
opendir(my $cwd, $dir);
for (readdir $cwd)
{
	next unless (s/(.*)\.pre/$1/);
	open(my $in, '<', "$_.pre");
	open(my $out, '>', "$_.htmlpart");
	my $buff = "";
	my $incode = 0;
	while (<$in>)
	{
		if ($incode)
		{
			if (m/\$endcode/)
			{
				$buff = highlight($buff);
				$buff =~ s/\t/&emsp;&emsp;/g;
				$buff =~ s/\n/<br \/>/g;
				print $out $buff;
				print $out "</p></div>\n";
				$buff = "";
				$incode = 0;
			}
			else
			{
				$buff .= $_;
			}
		}
		elsif (m/\$startcode/)
		{
			$incode = 1;
			print $out qq[<div class="cmcontainer"><p class="cm">\n];
		}
		else
		{
			s/`(.*?)`/<span class="inlinecode">$1<\/span>/g;
			s/\*(.*?)\*/<i>$1<\/i>/g;
			print $out $_;
		}
	}
	close $in;
	close $out;
}
closedir $cwd;

sub loadrules
{
	open(my $f, '<', "$dir/.rules")
		or die "IO error on .rules: $!";
	my %rules;
	while (<$f>)
	{
		chomp;
		my ($key, $value) = split /[\t|\s]+/, $_, 2;
		$key =~ s/,/ /g;
		$rules{$key} = $value;
	}
	return %rules;
}

sub highlight
{
	$_ = shift;
	
}
